import { Selector } from 'testcafe';
import { BasePage } from './base.page';
import { DashboardPage } from './dashboard.page';

export class LoginPage extends BasePage {

    private _usernameInput;
    private _passwordInput;
    private _loginButton;

    constructor(t: TestController) {
        super(t);
        this._usernameInput = Selector('input[name="user"]');
        this._passwordInput = Selector('input[name="password"]');
        this._loginButton = Selector('input[value="Login"]');
    }

    public async login(username: string, password: string): Promise<DashboardPage> {
        await this.t.typeText(this._usernameInput, username);
        await this.t.typeText(this._passwordInput, password);
        await this.t.click(this._loginButton);
        return new DashboardPage(this.t);
    }
}