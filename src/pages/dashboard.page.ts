import { BasePage } from "./base.page";
import { Selector } from "testcafe";

export class DashboardPage extends BasePage {

    private _dashboardTitle: Selector;

    constructor(t: TestController) {
        super(t);
        this._dashboardTitle = Selector('#available');
    }

    public async isDashboardTitlePresent(): Promise<boolean> {
        return await this._dashboardTitle.exists;
    }
}