import { Selector } from 'testcafe';
import * as fs from 'fs';
import { LoginPage } from '../src/pages/login.page';
import { DashboardPage } from '../src/pages/dashboard.page';

const users = JSON.parse(fs.readFileSync('./data/users.json', 'utf8'));
const mainUser = users.find(c => c.user === 'test');
let loginPage: LoginPage;
let dashboardPage: DashboardPage;

fixture `Getting started`
    .page `http://sahitest.com/demo/training/login.htm`;

// Login test in Sahitest page
test('Sahi login test', async(t) => {
    loginPage = new LoginPage(t);
    dashboardPage = await loginPage.login(mainUser.user, mainUser.password);
    await t.expect(await dashboardPage.isDashboardTitlePresent()).eql(true);
});